#!/usr/bin/env bash
pushd $(dirname $0) &> /dev/null
tester_dir=$(pwd)
popd &> /dev/null
pip3 install -q -U -r ${tester_dir}/resources/requirements.txt
${tester_dir}/PostmanTester.py $*
