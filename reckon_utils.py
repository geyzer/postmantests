"""
ReckonGate Utility functions
"""
import os
import re
import enum
import time
import json
import itertools
import copy

from functools import partial
import importlib.util as imp_util
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
import smtplib

non_words = re.compile('\W+')
tokenizer = partial(non_words.sub, '_')

def create_enum(func=lambda x: x):
    """
    Wraps convenient creation of Enum  with conversion function
    :param func: function to convert values
    :return: function that builds Enum object
    """
    def _enum_builder(container, *fields, **kw_consts):
        """
        Wraps Enum creation constants, use-cases below may be mixed
         - For list of strings - tokenized uppercase string->(converted) string
         - For keyword pairs - tokenized uppercase field name->(converted) value

         IMPORTANT! each string used as field name must be tokenizable,
                    i.e. used as source for valid Python ID,
                    spaces and alphanumeric literals, first character - non-numeric or underscore

        :param container: string name - unique!(discarded)
        :param fields: constant strings to be wrapped
        :param kw_consts: identifier-values pairs to be wrapped
        :return: Enum object
        """
        if kw_consts:
            kw_keys, kw_values = zip(*kw_consts.items())
        else:
            kw_keys = ()
            kw_values = ()
        return enum.Enum(container, zip([tokenizer(w.upper()) for w in fields + kw_keys],
                                        [func(f) for f in fields + kw_values]),
                         type=str)

    return _enum_builder


TS_FORMAT = '_%b-%d-%y_%Hh%Mm%Ss'
def name_with_timestamp(prefix, suffix='', with_seconds=True):
    """
    Produces file/folder name with timestamp

    :param prefix: path prefix
    :param suffix: file name suffix
    :param with_seconds: on False, remove seconds from format
    :return: name with timestamp
    """
    ts_fmt = with_seconds and TS_FORMAT or TS_FORMAT.rpartition('%')[0]
    return prefix + time.strftime(ts_fmt, time.localtime()) + suffix


def align_rows(table, column_sep='    ', multi_line=False):
    """
    Turns table - list of list - of data into table with aligned columns
    :param table: data to present
    :param column_sep: column separator
    :param multi_line: on True, expect multi-line content in cells
    :return: aligned table as string
    """
    if multi_line:
        # Rebuild table by split cells - complete missing sub-rows with spaces
        sub_rows = list([cell.split('\n') for cell in row] for row in table)
        table = list(itertools.chain.from_iterable(itertools.zip_longest(*sub_row, fillvalue='')
                                                   for sub_row in sub_rows))
    widths = (max(len(v) for v in col) for col in zip(*table))
    line_fmt = column_sep.join('{{:<{}}}'.format(width) for width in widths)
    return '\n'.join(line_fmt.format(*row) for row in table)


URL_RE = re.compile(r'http://[\w.]+:\d{4}')
def common_url_prefix(*url_list):
    """
    Find longest commmon prefix in list of URLs
    :param url_list: list of URL
    :return: longest common URL prefix - if found
    """
    shortest, *long_strings = sorted(url_list, key=lambda s: len(s))
    shortest = shortest.rstrip('/')
    while URL_RE.match(shortest):  # While URL is valid
        if all(shortest in s for s in long_strings):
            return shortest
        shortest = shortest.rpartition('/')[0]
    return ''


def json_within_string(source, default_value=None, _JSON_RE=re.compile(r'(?s)(\{|\[).+(\}|\])')):
    """
    Find JSON element in free text
    :param source: source string
    :param default_value: value to return if JSON not found
    :param _JSON_RE: RE to find JSON part
    :return: JSON object
    """
    try:
        return json.loads(_JSON_RE.search(source).group(0).replace("'", '"'))
    except (AttributeError, json.JSONDecodeError) as exc:
        return {} if default_value is None else default_value

def remove_json_xpath(json_obj, *xpath_elem):
    """
    Remove from JSON object elements addressed by xpath - sequence of keys
    :param json_obj: initial object
    :param xpath_elem: list of xpath keys in JSON object hierarchy
    :return: new JSON object with required xpath removed
    """
    source = copy.deepcopy(json_obj)
    if len(xpath_elem) == 1:
        source.pop(xpath_elem[0], None)
        return source
    head, *tail = xpath_elem
    updated_elem = remove_json_xpath(source[head], *tail)
    tail_key = xpath_elem[-1]
    if not updated_elem:
        del source[tail_key]
    else:
        source[tail_key] = updated_elem
    return source

def remove_multiple_json_xpaths(json_obj, *xpaths_list):
    """
    Remove multiple xpaths from JSON object
    :param json_obj: initial object
    :param xpaths_list: list of xpaths
    :return: new JSON object with required xpaths removed
    """
    for xpath in xpaths_list:
        json_obj = remove_json_xpath(json_obj, *xpath.split('/'))
    return json_obj


def find_values_for_key(json_obj, key):
    """
    Find values for keys that may be at varying depths in JSON object
    :param json_obj: source JSON object
    :param key: single key to find
    :return: values for key
    """
    def _deep_find(json_obj):
        """
        Find values for keys and place values in accumulator
        :param json_obj: source JSON object at current depth
        :return: None
        """
        if isinstance(json_obj, dict):
            try:
                accumulator.append(json_obj[key])
            except KeyError:
                for component in json_obj.values():
                    _deep_find(component)
        elif isinstance(json_obj, list):
            for component in json_obj:
                _deep_find(component)
    accumulator = []
    _deep_find(json_obj)
    return accumulator

def find_values_for_key_list(json_obj, *key_list):
    """
    Find combination of values in JSON objects by key_list
    :param json_obj:
    :param key_list:
    :return:
    """
    return list(itertools.product(*[find_values_for_key(json_obj, key) for key in key_list]))

def dynamic_import(module_name, path2module):
    """
    Dynamic import of module
    :param module_name: name of module
    :param path2module: path to module
    :return: module object
    """
    collection_spec = imp_util.spec_from_file_location(
        module_name, '{}/{}.py'.format(path2module, module_name))
    collection_module = imp_util.module_from_spec(collection_spec)
    collection_spec.loader.exec_module(collection_module)
    return collection_module

class MailClient(object):
    """
    Mail Client context manager
    """
    def __init__(self, sender, auth):
        """
        :param sender: sender user
        :param auth: sender credentials
        """
        self.sender = sender
        self.auth = auth

    def __enter__(self):
        """
        Create SMTP client and login sender
        :return: SMTP client instance
        """
        self.mail_client = smtplib.SMTP('smtp.gmail.com:587')
        self.mail_client.ehlo()
        self.mail_client.starttls()
        self.mail_client.login(self.sender, self.auth)
        return self.mail_client

    def __exit__(self, *_):
        """
        Close SMTP client connection
        :return: None
        """
        self.mail_client.close()


def send_mail(mail_client, from_id, to_list, subject, content, *attachments, content_type='html'):
    """
    Send MIME mail message
    :param mail_client: SMTP client
    :param from_id: ID of mail sender
    :param to_list: list of mail receivers
    :param subject: mail subject
    :param content: mail content
    :param attachments: mail attachments
    :param content_type: contents type
    :return: None
    """
    mail_obj = MIMEMultipart()
    mail_obj['From'] = from_id
    mail_obj['To'] = COMMASPACE.join(to_list)
    mail_obj['Date'] = formatdate(localtime=True)
    mail_obj['Subject'] = subject

    mail_obj.attach(MIMEText(content, content_type))

    for attachment_name in attachments:
        with open(attachment_name, "rb") as attachment_file:
            short_name = os.path.basename(attachment_name)
            attachment = MIMEApplication(attachment_file.read(), Name=short_name)
            attachment['Content-Disposition'] = 'attachment; filename="{}"'.format(short_name)
            mail_obj.attach(attachment)

    mail_client.sendmail(from_id, to_list,  mail_obj.as_string())
