#!/usr/bin/env python3

import os
import collections
import re
import pprint
import json

from lxml import etree

from reckon_utils import dynamic_import, json_within_string, align_rows, send_mail, MailClient
from test_consts import C, KW, RVAR, ROBOT_TYPED

############################################################
#            Definitions for mail
############################################################
QA_OWNER = 'mark@recongate.com'
SENDER = 'yaniv@recongate.com'
AUTH = 'cabkeavlfqgwmedb'
FROM = 'me@gmail.com'

FAILED_TEMPLATE = """\
<html>
  <head></head>
  <body>
    <p>Extracts from test run - logs attached</p>
    <p>The following tests failed:</p>
     <pre style="font: monospace">
     {}
  </body>
</html>
"""

SUCCESS_TEMPLATE= """\
 <html>
  <head></head>
  <body>
    <p>The following test suite(s) from Postman QA collection passed:</p>
     <pre style="color:red">
     {}
  </body>
</html>
"""

SEP = '\n' + '-' * 30 + '\n'

FAIL_ELEMS = ('Test', 'Failed Step', 'Reason', 'Method', 'Request URL', 'Request Data', 'Response Data')
HTML_COL = lambda s, col='red': '<span style="color:{}">{}</span>'.format(col, s)

REST_KW_MISSING = '''
Probable failure reason - automation bug or previous suite failures
Reported failure: {}
'''

LOG_FILES = 'report.html', 'log.html', 'output.xml'

############################################################
#          Test Suite log processing
############################################################
EFIELD = lambda e, field_name=C.NAME: e.attrib[field_name]
REST_IN_MSG = re.compile(r'(http\S+) "(\w+) (\S+)')
MSG_BY_LVL = lambda kw, lvl=C.FAIL: (m for m in kw.iter(C.MSG) if EFIELD(m, C.LEVEL) == lvl)
PRINT_DATA = lambda v: v if isinstance(v, str) else pprint.pformat(v, width=60)


def test_by_status(main_tree, required_status=C.FAIL):
    """
    Test generator - by status
    :param main_tree: Test Suite XML tree root
    :param required_status: Status to yield by
    :return: suite element, test element
    """
    for suite in main_tree.xpath(C.SUITE)[0].xpath(C.SUITE):
        for test in suite.xpath(C.TEST):
            if test.xpath(C.STATUS)[0].attrib[C.STATUS] == required_status:
                yield suite, test

class SuitePostProcessing(object):
    """
    Suite output processing - analyze failures and send reports to developers responsible for collection
    """
    def __init__(self, results_dir, debug=False):
        """
        :param results_dir: Folder with test results
        """
        self.results_dir = results_dir
        self.guilty = collections.defaultdict(list)
        self.variable_modules = {}
        self.result_tree = etree.parse(results_dir + '/output.xml')
        self.debug = debug

    @property
    def log_list(self):
        """
        List of log files - full paths
        :return: list of paths
        """
        return ['{}/{}'.format(self.results_dir, log_name) for log_name in LOG_FILES]

    def get_variable_module(self, suite_elem):
        """
        Get module with suite variables
        :param suite_elem: suite element
        :return: module object
        """
        suite_name = EFIELD(suite_elem)
        return self.variable_modules.setdefault(
            suite_name, dynamic_import(suite_name, os.path.dirname(EFIELD(suite_elem, C.SOURCE))))

    def get_suite_owner(self, suite_elem):
        """
        Get e-mail of suite owner - if available
        :param suite_elem: suite element
        :return: e-mail address
        """
        suite_vars_module = self.get_variable_module(suite_elem)
        return getattr(suite_vars_module, C.DESCRIPTION, {}).get(C.OWNER, C.ORPHAN)

    def handle_suite_failures(self):
        """
        Processes suite failures and extracts all relevant information for each failed test
        :return: None
        """
        for failed_suite, failed_test in test_by_status(self.result_tree):
            suite_name = EFIELD(failed_suite)
            owner = self.get_suite_owner(failed_suite)
            suite_vars_module = self.get_variable_module(failed_suite)
            error_record = self.handle_test_failure(failed_test, suite_vars_module)
            test_name = HTML_COL(EFIELD(failed_test))
            self.guilty[(owner, suite_name)].append((test_name,) + error_record)

    def handle_test_failure(self, failed_test, suite_vars_module):
        """
        Processes individual test failures
        :param failed_test: failed test elemet
        :param suite_vars_module: module object with suite variables
        :return:
        """
        all_test_kw = failed_test.xpath(C.KW)
        failed_kw = all_test_kw[-1]
        failed_step_name = EFIELD(failed_kw)

        # Try to find REST keyword in test flow
        rest_kw = next((kw for kw in all_test_kw
                       if EFIELD(kw) == KW.REST_REQUEST_WITH_STATUS_VALIDATION), None)
        if rest_kw is None:
            return failed_step_name, REST_KW_MISSING.format(next(MSG_BY_LVL(failed_kw)).text.strip())

        # Extract relevant to report data from test element "children"
        target_level = rest_kw == failed_kw and C.FAIL or C.INFO
        rest_msg_text = next((m.text for m in rest_kw.xpath(C.MSG)
                              if EFIELD(m, C.LEVEL) == target_level), '')
        fail_msg = next(MSG_BY_LVL(failed_kw)).text.partition(C.RESPONSE_CONTENT)[0].strip()
        resp_data = json_within_string(rest_msg_text.partition(' ')[-1], default_value='N/A')
        method, url, json_var = [arg.text for arg in rest_kw.find('arguments').findall('arg')[:3]]
        req_data = getattr(suite_vars_module, json_var[2:-1], 'N/A')
        url = url.replace(RVAR(C.URL_PREFIX), getattr(suite_vars_module, C.URL_PREFIX))

        # Collect suite random variables - if exist
        random_vars_text = next((m.text for kw in all_test_kw if 'Random' in kw.attrib['name']
                               for m in kw.xpath('msg') if '=' in m.text), None)
        if random_vars_text is not None:
            # Try to resolve variables - either URL components or request data
            variable_ids, variable_values = list(v.split() for v in random_vars_text.split('='))
            if len(variable_ids) == 1:
                variable_values = [' '.join(variable_values)]
            random_vars = dict(zip(variable_ids, variable_values))
            url = '/'.join([random_vars[c] if '$' in c else c for c in url.split('/')])
            if req_data == 'N/A' and json_var != ROBOT_TYPED(None):
                # Occasionally, request data are set as random variables!!
                req_data = json.loads(random_vars[json_var].replace("\'", '"'))

        return failed_step_name, fail_msg, method, url, PRINT_DATA(req_data), PRINT_DATA(resp_data)


    def send_error_notifications(self):
        """
        Iterates over failure data and sends mails to relevant developer(s)
        :return: None
        """
        if not self.guilty:
            self.handle_suite_failures()

        with MailClient(SENDER, AUTH) as mail_client:
            for (owner, suite_name), failure_list in self.guilty.items():
                if owner == C.ORPHAN or self.debug:
                    to_list = (QA_OWNER, )
                else:
                    to_list = (owner, QA_OWNER)

                # Build error info tables
                fail_info = ''
                for failure_rec in failure_list:
                    # Add failure record
                    fail_info += SEP
                    fail_info += align_rows(list(zip(FAIL_ELEMS, failure_rec)), multi_line=True)

                send_mail(mail_client, FROM, to_list,
                          'Failures in Test Suite "{}" - based on Postman QA collection'.format(suite_name),
                          FAILED_TEMPLATE.format(fail_info), *self.log_list)

    def report_successes(self):
        """
        Inform owner of passed suites
        :return:
        """
        successes = collections.defaultdict(list)
        for suite in self.result_tree.xpath(C.SUITE)[0].xpath(C.SUITE):
            if suite.xpath(C.STATUS)[0].attrib[C.STATUS] != C.PASS:
                continue
            successes[self.get_suite_owner(suite)].append(EFIELD(suite))

        with MailClient(SENDER, AUTH) as mail_client:
            for owner, suite_list in successes.items():
                if owner == C.ORPHAN or self.debug:
                    to_list = (QA_OWNER, )
                else:
                    to_list = (owner, QA_OWNER)
                send_mail(mail_client, FROM, to_list, 'Congratulations! Your collections passed tests!',
                          SUCCESS_TEMPLATE.format(', '.join(suite_list)),  *self.log_list)



if __name__ == '__main__':
    import sys
    res = SuitePostProcessing(sys.argv[1], debug=True)
    res.send_error_notifications()
    res.report_successes()
