"""
Test management and string constants
"""
import os
from reckon_utils import create_enum

# Helper function to format Robot variable in line
RVAR = lambda v: '${{{}}}'.format(v)
# Helper function to quote value
QUOTE = lambda v, q='"': '{0}{1}{0}'.format(q, v)

# Type "decoration" for "int" ("bool" - as "int" subset) and None values in Robot script
# Any value passed "un-decorated" to Robot script is processed as string
ROBOT_TYPED = lambda v: (isinstance(v, int) or v is None) and RVAR(repr(v).upper()) or v

_path = os.path.abspath(os.path.dirname(__file__))
_resources = _path + '/resources/'

# Global constants
C = create_enum()('consts',
                  'environments', 'collections', 'production', 'qa', 'local',
                  'uid', 'key', 'value', 'values',
                  'request', 'item', 'name', 'body', 'raw', 'url', 'method', 'header', 'json',
                  'headers', 'REST Response', 'description', 'id', 'external_var',
                  'URL_PREFIX', 'expected_value', 'lookup_id', 'as_list', 'Response Content', 'Owner',
                  # Supported Suites
                  'Cases', 'InvestigationFlow', 'Keywords', 'Octopus', 'Avatars',
                  # Supported REST methods
                  'POST', 'GET', 'PUT', 'DELETE', 'PATCH',
                  # Common objects
                  'CREATED_OBJS', 'DELETED_OBJS', 'ID_LIST',
                  # Robot Keyword tags
                  'rest', 'validation', 'nontest',
                  # Post-processing - XML and common keywords
                  'INFO', 'DEBUG', 'FAIL', 'PASS',
                  'level', 'suite', 'test', 'msg', 'tags', 'source', 'kw', 'status', 'orphan',
                  PATH=_path,
                  RESOURCES=_resources,
                  POSTMAN_API_KEY=open(_resources+'postman-key.txt').read().strip(),
                  GIT_KEYFILE=_resources + 'devops-recongate.com.pem',
                  SERVICES_KEY = _resources + 'AWS-Ireland.pem',
                  ROBOTS = _path + '/Robots/')

DEFAULT_HEADER = {'Content-Type': 'application/json'}

# Key Words for test script
KW = create_enum()('ROBOT_KW',
                   'Rest Request With Status Validation',
                   'Send Request',
                   'Save Result',
                   'Compare Fields',
                   'Validate Response',
                   'Fetch From Dict',
                   'Dict From List',
                   'Dict Contains',
                   'Store Created Object',
                   'Select Random Created Object',
                   'Select Source Dict',
                   'Update Expected Record',
                   'Clean Test Created Objects',
                   'Object Components Valid',
                   'Object Field Valid',
                   'Validate Response Object Against Stored',
                   'Validate Created Objects in Bulk Query',
                   'Remove Deleted Object',
                   'Set Random URL Components',
                   'Validate Object Mappings In Response',
                   'Store Property Mappings',
                   'Validate Object List In Response',
                   'Field From Random Object',
                   'Validate Bulk Deleted Objects'
                   )

TC_SECTIONS = create_enum(lambda s: '*** {} ***'.format(s)) \
    ('tc_sections', 'Settings', 'Keywords', 'Test Cases', 'Variables')

POSTMAN_DOC = ('Postman Test Suite',
               'Results/logs dumped in "${DUMP_DIR}')

OBJECTS_CLEANUP = 'Suite Teardown      {}'.format(
                  '  '.join((KW.CLEAN_TEST_CREATED_OBJECTS, RVAR(C.URL_PREFIX))))
