#!/usr/bin/env python3
"""
Upper-level module Postman Tests
It builds Robot scripts for the Test Suite(s) and starts the test
"""

import sys, os
import argparse
import json
import subprocess
import requests
from robot.run import run as robot_run

from test_consts import C, ROBOT_TYPED, POSTMAN_DOC
from robot_utils import resolve_object_variables
from collection_utils.collection_base import collection_factory
from reckon_utils import tokenizer, name_with_timestamp, json_within_string
from suite_results import SuitePostProcessing

MSG_FMT = '\x1b[31m{} \x1b[1;3m{}\x1b[0m\n'

def postman_get(obj, uid='', key=C.POSTMAN_API_KEY):
    url='https://api.getpostman.com/{}/{}'.format(obj, uid)
    headers={'x-api-key': key}
    return list(requests.request('GET', url, headers=headers, timeout=10).json().values())[0]


def postman_collection(collections_uid, name):
    collection_uid = next(collection[C.UID] for collection in collections_uid
                          if name == collection[C.NAME])
    return postman_get(C.COLLECTIONS, uid=collection_uid)


def save_collection(folder, name, collection):
    """
    Save collection - for reference/debug purpose
    :param name: Collection name
    :param collection: Collection value
    :return: None
    """
    with open('{}/{}.json'.format(folder, tokenizer(name)), 'w') as json_file:
        json.dump(collection, json_file)


def cli_parser(cli_args):
    parser = argparse.ArgumentParser(prog=os.path.abspath(__file__),
                                     description='Postman Tester'
                                     'Test APIs defined in Postman Collections',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('workdir', type=str,
                        help='Parent folder for local repositories and test output.\n'
                             'To re-use existing local repositories, use their parent folder\n'
                             'as "workdir" argument and omit "--clone" argument')
    parser.add_argument('suites', type=str, nargs='*',
                        help='Suite(s) to be tested')
    # parser.add_argument('--clone', action='store_true', default=False, dest='clone',
    #                     help='Clone scraper components (default-False).\n'
    #                          'Caution! May wipe local repositories')
    # parser.add_argument('--validate', default=False, action='store_true', dest='validate',
    #                     help='Validate scraping results agains schema (default-False)')
    parser.add_argument('-e', '--postman-env', default=C.QA, dest='postman_environment',
                        choices=(C.LOCAL, C.PRODUCTION, C.QA),
                        help='Postman environment to use in tests - default "{}"'.format(C.PRODUCTION))
    parser.add_argument('--dry-run', default=False, dest='dry_run', action='store_true',
                        help='Create test suites, but don\'t run Robot')

    parser.add_argument('--debug', default=False, dest='debug', action='store_true',
                        help='Send notifications to QA developer only')

    parser.add_argument('-a', '--get-attempts', type=int, default=3, dest='get_retries',
                        help='Number of attempts to run GET requests with verification before failing test.\n'
                             'Used to account for race condition in preceding POST/PUT/DELETE requests')

    args = parser.parse_args(cli_args)

    tests_dump = name_with_timestamp('{}/TestDump'.format(args.workdir.rstrip('/')),
                                     with_seconds=False)
    try:
        os.makedirs(tests_dump)
    except OSError as os_err:
        parser.error('Can\'t create dump folder "{}", reason - {}'.format(tests_dump, os_err.strerror))
    tests_dump = os.path.abspath(tests_dump)
    if args.dry_run:
        robots_path = C.ROBOTS
    else:
        robots_path = tests_dump + '/PostmanCollections/'
        # Attach helper file to run collections
        helper_script = tests_dump + '/runtests.sh'
        pybot = subprocess.check_output(['which', 'pybot']).decode().strip()
        with open(helper_script, 'w') as helper_file:
             helper_file.write('#!/usr/bin/env bash \n'
                               'rundir={}\npybot={}\nif [[ $# == 0  ]] ; then \n$pybot $rundir \n '
                               'else for coll in $@ ; do $pybot ${{rundir}}/${{coll}} ; done '
                              '\nfi\n'.format(robots_path, pybot))
        os.chmod(helper_script, 0x555)

    setattr(args, 'robots', robots_path)

    if not os.path.exists(robots_path):
        os.mkdir(robots_path, 0o700)
    else:
        subprocess.call("find {} \( -name '*.json' -o -name '*.robot' -o -name '*.py' \) -delete".format(robots_path),
                        shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    git_key = '{}{}'.format(robots_path, os.path.basename(C.GIT_KEYFILE))
    if not os.path.exists(git_key):
        make_key = subprocess.Popen('cp {} {} &&  chmod 0500 {}'.format(C.GIT_KEYFILE, robots_path, git_key),
                                   shell=True, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
        err_msg = make_key.communicate()[1]
        if make_key.returncode != 0:
            parser.error('Failed to create GIT key {}: {}'.format(git_key, err_msg))
    setattr(args, 'git_key', git_key)

    return args, tests_dump

if __name__ == '__main__':
    """
    Generate robot script by provided requests and run test suite by robot
    """
    args, tests_dump = cli_parser(sys.argv[1:])

    all_environments = postman_get(C.ENVIRONMENTS)
    test_env_uid = next(e[C.UID] for e in all_environments
                        if e[C.NAME] == args.postman_environment)

    work_env = {e[C.KEY]: e[C.VALUE] for e in postman_get(C.ENVIRONMENTS, test_env_uid)[C.VALUES]}
    save_collection(tests_dump, args.postman_environment, work_env)

    all_collections = postman_get(C.COLLECTIONS)
    collection = postman_collection(all_collections, 'QA')
    save_collection(tests_dump, 'QA', collection)
    collection = resolve_object_variables(collection, work_env)
    save_collection(tests_dump, 'QA' + '_resolved', collection)
    suite_items = collection[C.ITEM]
    suites = [c[C.NAME] for c in suite_items] if not args.suites else args.suites

    suite_cnt = 0
    resources_file = args.robots + 'resources.robot'
    for suite_name in suites:  # C.CASES, C.AVATARS, C.INVESTIGATIONFLOW, C.KEYWORDS):
        try:
            suite_requests, suite_descr = next((s[C.ITEM], s[C.DESCRIPTION])
                                               for s in suite_items if s[C.NAME] == suite_name)
        except StopIteration:
            print(MSG_FMT.format('Skipping misnamed test suite', suite_name))
            continue

        suite_cnt += 1
        suite_descr = json_within_string(suite_descr)
        collection_obj = collection_factory(suite_name,
                                            tests_dump, args.robots, args.git_key, args.get_retries, suite_descr)
        collection_obj.create_file_testsuite(suite_name, suite_requests, resources_file)

    if suite_cnt:
        collection_obj = collection_factory(None, tests_dump, args.robots)
        collection_obj.libraries.extend((C.PATH + '/TestHelper.py  ${DUMP_DIR}', 'Collections', 'BuiltIn'))
        collection_obj.variables.extend((('DUMP_DIR', tests_dump), ('BASE_DIR', C.PATH)))
        collection_obj.write_testsuite_file('resources')
        collection_obj.resources.append(resources_file)
        collection_obj.documents.extend(POSTMAN_DOC)
        collection_obj.write_testsuite_file('__init__')

        if not args.dry_run:
            robot_run(args.robots, outputdir=tests_dump, loglevel='DEBUG', reportbackground='green:yellow:grey')
            post_run = SuitePostProcessing(tests_dump, debug=args.debug)
            post_run.handle_suite_failures()
            post_run.send_error_notifications()
            post_run.report_successes()
        else:
            print(MSG_FMT.format('Dry run ended; Robots available at ', args.robots))
    else:
        print(MSG_FMT.format('', 'No valid suites found!!!'))
