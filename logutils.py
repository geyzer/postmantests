import time
import os
import subprocess
import multiprocessing

POLL_MSG = '{res} - wasit for "{poll_str}" in "{log_src}" for {dur}secs'

class LogBase(object):
    def __init__(self, testapp_logger):
        self.__target_line = ''
        self.lookup_start = 0
        self.testapp_logger = testapp_logger
        self.__queue = multiprocessing.Queue()

    def lookup(self, poll_str, poll_cycles=1, timestamp=None, move_pos=False, queue_res=False):
        """
        Lookup for a line containing specific string in remote application log

        :param poll_str: string to poll for
        :param poll_cycles: maximum number of poll cycles
        :param move_pos: whether to move starting lookup point on return
        :return: line containing poll string
        """
        self.__target_line = ''
        log_source = self.get_log_source()
        log_kwargs = dict(poll_str=poll_str, log_src=self.log_src)

        for elapsed in range(poll_cycles):
            with open(log_source, 'rb') as log_file:
                log_file.seek(self.lookup_start)
                try:
                    target_line = next(line.strip() for line in log_file.readlines()
                                       if poll_str in line)
                    self.testapp_logger.info(POLL_MSG.format(res='Passed', dur=elapsed, **log_kwargs))
                    break
                except StopIteration:
                    pass
                time.sleep(1)
        else:
            self.testapp_logger.error(POLL_MSG.format(res='Failed', dur=poll_cycles, **log_kwargs))
            target_line = ''

        self.__target_line = target_line
        if move_pos:
            self.set_timestamp()
        self.close_log()
        return target_line

    @property
    def lookup_res(self):
        return self.__target_line


class LocalLogUtils(LogBase):
    """
    Handler object for application log processing
    """

    def __init__(self, testapp_logger, log_name='', **_kwargs):
        """

        :param log_name: name of client apllication log
        :param testapp_logger: logger of test app
        """
        super(RemoteLogUtils, self).__init__(testapp_logger)
        self.__log_name = log_name
        self.__log_src = log_name

    def get_log_source(self):
        return self.__log_name

    def close_log_source(self):
        pass

    def get_timestamp(self):
        self.lookup_start = os.stat(self.__log_name).st_size

    def set_timestamp(self):
        self.lookup_start = self.get_timestamp()



# Template for log file of remote applications local stdout redirect
LOCAL_MIRROR = '/tmp/Mirror-%s.log'

class RemoteLogUtils(object):
    """
    Handler object for remote application log processing
    Remote app log is re-directed to local mirror file -
    with timestamp in name
    """

    def __init__(self, testapp_logger, hostname='', service='', **_kwargs):
        """

        :param log_name: name of client apllication log
        :param testapp_logger: logger of test app
        """
        super(RemoteLogUtils, self).__init__(testapp_logger)
        self.__hostname = hostname
        self.__service = service
        self.__cmd_wrapper = 'ssh ubuntu@{} "{{}}"'.format(hostname)
        self.__log_follow_cmd = 'ssh ubuntu@{} "docker logs --since {{ts}} {{srvc}}" > {{mirror}}'.format(hostname)
        self.__log_src = service


    def get_log_source(self, timestamp=None):
        log_mirror = time.strftime(LOCAL_MIRROR, time.localtime())
        self.follow_pobj = subprocess.Popen(
            self.__log_follow_cmd.format(ts=timestamp or self.get_timestamp(), srvc=self.__service, mirror=log_mirror),
            shell=True)
        return log_mirror


    def close_log(self):
        if not self.follow_pobj is None:
            self.follow_pobj.kill()
            self.follow_pobj = None


    def get_timestamp(self):
        return subprocess.check_output(self.__cmd_wrapper.format('date +%Y-%m-%dT%H:%M:%S.000Z'), shell=True)


    def set_timestamp(self):
        self.timestamp = self.get_timestamp()


class LogMonitor(object):
    def __init__(self, logger_obj, *args, **kwargs):
        self.__args = args
        kwargs['queue_res'] = True
        self.__kwargs = kwargs
        self.__logger_obj = logger_obj


    def __enter__(self, *args, **kwargs):
        self.__lookup_process = multiprocessing.Process(target=self.lookup,
                                                        args=self.args, kwargs=self.kwargs)
        self.__lookup_process.start()

        def __exit__(self):
            self.__lookup_process.join()
            self.__logger_obj.close_log()


def logutils_factory(testapp_logger, remote=False, **kwargs):
    return (remote and RemoteLogUtils or LocalLogUtils)(testapp_logger, **kwargs)

