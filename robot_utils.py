"""
Robot-specific utility functions
"""
import json
import re
import time
import random

from test_consts import KW

LOG_NAME = 'ROBOT'
from robot.api.logger import logging
logger_obj = logging.getLogger(LOG_NAME)

def keywords_missing(*args, **kwargs):
    return 'Expected keywords - [{}], got - [{}]'.format(
        *(', '.join(a) for a in (args, kwargs)))

def add2intest_vars(intest_vars, var_name):
    """
    Add in-test variable to dict of in-test variables
    Used to substitute unresolved variables in test steps
    :param var_name: name of in-test variable
    :return: copy of original with variable added
    """
    intest_copy = intest_vars.copy()
    intest_copy.update({var_name: RVAR(var_name)})
    return intest_copy


def random_id_from_time_hash(dashmap=(8, 4, 4, 4, 12)):
    """
    Create random ID of hex-value, separated by dashes according to map
    :param dashmap: lengths of fragments
    :return: random value string
    """
    random_val = ''
    while len(random_val) < sum(dashmap):
        random_val += '{:016x}'.format(hash(time.time() * random.randint(1, 1000)))
    return '-'.join(re.findall(''.join('(\S{{{}}})'.format(l) for l in dashmap), random_val)[0])


POSTMAN_VAR = re.compile(r'{{\w+}}')
def resolve_object_variables(source, environment, exact_match=True):
    """
    Resolve Postman variables in JSON source - variable is ID surrounded by
    double curly brackets - by values provided in "environment" object
    :param source: JSON opbject with (potentially) unresolved
    :param environment: dictionary of ID-> substitution value
    :return: JSON object with resolved available variables
    """
    def env_sub(match_obj):
        """
        Provides variable substitution - if possible
        :param match_obj: match object from RE
        :return: resolved name - or original value
        """
        var_name = match_obj.group(0)
        var_value = environment.get(var_name.strip('{}'), var_name)
        if var_value == var_name and (not exact_match or 'random' in var_name):
            var_value = random_id_from_time_hash()
        return var_value

    source_str = json.dumps(source)
    resolved_str = POSTMAN_VAR.sub(env_sub, source_str)
    return json.loads(resolved_str)

STORED_OBJ_RE = re.compile(r'{}\s+"(.+)"\s+\${{([^}}]+)}}'.format(KW.STORE_CREATED_OBJECT))
def stored_object_name(robot_line):
    return STORED_OBJ_RE.findall(robot_line)
