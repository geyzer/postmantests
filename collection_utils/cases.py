"""
Contains class for Cases collection
"""

from collection_utils import collection_base
from test_consts import C, OBJECTS_CLEANUP

class Cases(collection_base.CollectionBase):
    ID_FIELD = C.ID

    def prepare_suite(self):
        super(Cases, self).prepare_suite()
        self.suite_setup.append(OBJECTS_CLEANUP)
