
from test_consts import C
from collection_utils import collection_base

class Flow(collection_base.CollectionBase):
    ID_FIELD = C.ID
    COMPONENT_PATH = 'investigation'

    def post_method(self, request, obj_name):
        return super(Flow, self).post_method(
            request, obj_name, verified_xpath=self.COMPONENT_PATH)
