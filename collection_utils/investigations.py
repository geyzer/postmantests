"""
Contains class for Flow collection
"""

from collection_utils import collection_base
from test_consts import C

class Investigations(collection_base.CollectionBase):
    ID_FIELD = C.ID

