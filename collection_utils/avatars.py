"""
Contains class for Avatars collection
"""

from collection_utils import collection_base
from test_consts import C, RVAR, QUOTE
from reckon_utils import create_enum
from robot_utils import stored_object_name

# Local constants
_C = create_enum()('AVATARS_CONSTS',
                     'all_avatars', 'avatars', 'ranger', 'deepCover',
                     'deepCover_list', 'ranger_list', 'provision',
                     'BY_SOURCE_N_TYPE', 'BY_SOURCE', 'BY_CASE_ID', 'BY_TYPE')

_F = create_enum()('AVATARS_FIELDS',
                   'avatar_type', 'domain', 'bio', 'image', 'octopus',
                   'container', 'avatar_id', 'case_id')

class Avatars(collection_base.CollectionBase):
    ID_FIELD = _F.AVATAR_ID
    EXCLUDED_XPATHS = ('{}/{}'.format(_F.BIO, _F.IMAGE),
                       _F.OCTOPUS, _F.CONTAINER)
    DICTIONARIES = {
        _C.BY_TYPE: (_F.AVATAR_TYPE,),
        _C.BY_SOURCE_N_TYPE: (_F.DOMAIN, _F.AVATAR_TYPE),
        _C.BY_CASE_ID: (_F.CASE_ID, ),
        _C.BY_SOURCE: (_F.DOMAIN, )
    }

    def avatar_type(self, request):
        try:
            avatar_type = next(av_type for av_type in (_C.DEEPCOVER, _C.RANGER)
                               if av_type in request[C.URL.lower()])
        except StopIteration:
            avatar_type = None
        return avatar_type


    def prepare_suite(self):
        self.variables.extend(collection_base.ROBOT_VARIABLE(name, {})
                              for name in self.DICTIONARIES)
        self.variables.extend(collection_base.ROBOT_VARIABLE(name, {})
                              for name in (_C.DEEPCOVER, _C.RANGER))


    def post_request_actions(self, _, req_steps):
        for step in req_steps:
            stored_obj_descr = stored_object_name(step)
            if stored_obj_descr:
                break
        else:
            return ()
        obj_descr, var_name = stored_obj_descr[0]
        return [self.store_mapping_step(obj_descr, obj_name=var_name, dict_name=dict_obj,
                                        component_keys=key_tuple)
                for dict_obj, key_tuple in self.DICTIONARIES.items()]


    def post_method(self, request, obj_description):
        body_var = request[C.EXTERNAL_VAR]
        #TODO - handle rangers and provisioning
        if body_var and 'file' in body_var.value or 'provision' in request[C.URL].lower():
            return (), ()
        steps = [self.make_request_record(request, expected_rc=201)]
        body_var = request[C.EXTERNAL_VAR]
        steps.append(self.validate_subdictionary_step(body_var.name, 'POSTed ' + obj_description))
        steps.append(self.store_object_step(obj_description))
        steps.append(self.store_object_step('DeepCover Avatar', dict_name=_C.DEEPCOVER))
        return steps, [body_var]


    def avatars_get_random_id_steps(self, parent_method, request):
        steps = []
        avatar_type = self.avatar_type(request)
        if 'negative' not in request[C.NAME].lower():
            obj_description = avatar_type +' Avatar'
            selection_step, request, id_var = self.random_existing_id(request, obj_description,
                                                                      source_dict=avatar_type)
            steps.append(selection_step)
        else:
            avatar_type = None
            obj_description = 'Avatar'
        request_steps, variables = \
            parent_method(request, obj_description, pick_id=False)
        steps.extend(request_steps)
        return steps, variables, avatar_type


    def put_method(self, request, obj_description):
        if 'provision' in request[C.URL].lower():
            # TODO: handle provisioning
            return (), ()
        steps, variables, avatar_type = \
            self.avatars_get_random_id_steps(super(Avatars, self).put_method, request)
        if not avatar_type is None:
            steps.append(self.store_object_step(obj_description, dict_name=avatar_type))
        return steps, variables


    def get_method(self, request, obj_description):
        if 'provision' in request[C.URL].lower():
            # TODO: handle provisioning
            return (), ()
        non_common_url = request[C.URL].replace(RVAR(C.URL_PREFIX), '').strip('/')
        avatar_type = self.avatar_type(request)
        url_components = non_common_url.split('/')
        if non_common_url == '':
            obj_description = 'All avatars'
            steps, variables = super(Avatars, self).get_method(request, obj_description)
        elif not avatar_type is None:
            steps, variables = super(Avatars, self).get_method(request, avatar_type)
        elif url_components[0] == C.ID:
            steps, variables, avatar_type = \
                self.avatars_get_random_id_steps(super(Avatars, self).get_method, request)
        else:
            source_dict = None
            if _F.CASE_ID in url_components:
                source_dict = _C.BY_CASE_ID
            elif url_components[-2] == 'source':
                source_dict = _C.BY_SOURCE
            elif url_components[-3] == 'source':
                source_dict = _C.BY_SOURCE_N_TYPE
            key_length = len(self.DICTIONARIES[source_dict])
            mapping_assign_step, mapping_verify_step, non_common_url = \
                self.random_existing_path(source_dict, url_components, key_length)
            request[C.URL] = '{}/{}'.format(RVAR(C.URL_PREFIX), non_common_url)
            obj_description = 'Avatars ' + source_dict
            steps, variables = super(Avatars, self).get_method(request, obj_description,
                                                               pick_id=False)
            steps.insert(0, mapping_assign_step)
            steps.append(mapping_verify_step)
        return steps, variables

    def delete_method(self, request, obj_description):
        return (), ()
