"""
Provides base class for creation of Robot-based test suites
based on Postman collections
"""
import os
import subprocess
import json
import pprint
import collections

from reckon_utils import tokenizer, align_rows, common_url_prefix, dynamic_import
from test_consts import C, DEFAULT_HEADER, KW, TC_SECTIONS, ROBOT_TYPED, RVAR, QUOTE


def collection_factory(class_name, *args, **kwargs):
    """
    Factory of collection-specific objects
    :param class_name: name of source module and of class to load
    :param args: class initialization positional arguments
    :param kwargs: class initialization keyword arguments
    :return: initialized collection object
    """
    if class_name is None:
        # Used for upper-level Robot environment creation
        collection_cls = CollectionBase
    else:
        # Specific classes - class per collection
        module_name = class_name.lower()
        collection_module = dynamic_import(module_name, os.path.dirname(__file__))
        collection_cls = getattr(collection_module, class_name)
    return collection_cls(*args, **kwargs)


GIT_WRAPPER = "ssh-agent bash -c 'ssh-add {key} ; {cmd}'"
CLONE_CMD = "git clone git@bitbucket.org:recongate-main/{repo}.git {target}"
FILEGET_CMD = 'git archive --remote=git@bitbucket.org:recongate-main/{repo} {branch} {path}'

ROBOT_VARIABLE = collections.namedtuple('EXTERNAL_VAR', 'name value')
TAG_LINE = '    '.join(('[Tags]', C.REST, C.VALIDATION, C.NONTEST))

class CollectionBase(object):
    """
    Base class for collections objects.
    - Defines common interfaces for building Postman test cases -
      which may be overloaded by specific collections.
    - Defines stubs for specific interfaces that may be implemented by some collection objects
    """
    #############################################################
    #    Defintions for REST request body formatting
    #############################################################
    # Fields of request
    REQ_FIELDS = (C.METHOD, C.URL, C.BODY, C.HEADERS)
    # Request parameters - positional arguments only
    REQ_FMT_POS = '  '.join('{' + f+ '}' for f in REQ_FIELDS)
    # Request - argument string for Keywords section
    # REQ_ARGS = '[Arguments]    ' +   + '  ${expected_rc}'

    REPO_NAME = None
    SCHEMA_PATH = None
    ID_FIELD = None
    RESOURCES = ()
    EXCLUDED_XPATHS = ()

    def __init__(self, dump_dir, robots_dir, git_key=None, get_retries=3, suite_descr=None, **_):
        """
        :param dump_dir: Folder to dump test objects in
        :param git_key: key for git access
        :param _: ignored
        """
        self.dump_dir = dump_dir.rstrip('/') + '/'
        self.git_key = git_key
        self.testbase_dir = robots_dir.rstrip('/') + '/'
        if self.SCHEMA_PATH:
            self.fetch_schema()
        self.common_url_prefix = ''
        self.get_retries = get_retries
        self.suite_descr = suite_descr
        self.resources = []
        self.keywords = []
        self.variables = []
        self.documents = []
        self.libraries = []
        self.test_cases = []
        self.suite_setup = []

    def clean_suite_components(self):
        self.resources.clear()
        self.keywords.clear()
        self.variables.clear()
        self.documents.clear()
        self.libraries.clear()
        self.test_cases.clear()
        self.suite_setup.clear()

    def clone_lib(self, repo_name, branch='HEAD'):
        repo_dir = self.testbase_dir + repo_name
        if os.path.exists(repo_dir):
            assert subprocess.call(['rm', '-rf', repo_dir], stderr=subprocess.PIPE), \
                'Failed to remove old repository "{}'.format(repo_dir)

        git_command = CLONE_CMD.format(repo=repo_name, target=self.__base_dir)
        clone_process = subprocess.Popen(GIT_WRAPPER.format(key=self.git_key, cmd=git_command),
                                         stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        clone_stderr = clone_process.communicate()[1]
        assert clone_process.returncode == 0, \
            'Failed to clone project "{}" - {}'.format(repo_name, clone_stderr)


    def fetch_file(self, repo_name, file_path, branch='HEAD'):
        """
        Fetch file from repository
        :param repo_name: name of repository
        :param schema_path: relative path of file
        :param branch: repository branch
        :return: file content
        """
        fetch_cmd = FILEGET_CMD.format(repo=repo_name, branch=branch, path=file_path)
        fetch_process = subprocess.Popen(GIT_WRAPPER.format(key=self.git_key, cmd=fetch_cmd),
                                         stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        content, err_msg = fetch_process.communicate()
        assert fetch_process.returncode == 0, \
            'Failed to fetch "{}", branch "{}" of repository "{}", reason - {}'.format(
                file_path, branch, repo_name, err_msg)
        # Content is surrounded by command output "garbage" - return "cleaned" content
        return str(content.rstrip(b'\x00').rpartition(b'\x00')[-1])


    def extract_schema(self, schema):
        """
        Stub for cuistom schema extraction (if required)
        :param schema: original schema
        :return: svhema - unchanged
        """
        return schema


    def fetch_schema(self, repo_name='', schema_path='', branch='HEAD'):
        """
        Fetch schema from repository for validations and store it in file
        :param repo_name: name of repository
        :param schema_path: relative path of schema file
        :param branch: repository branch
        :return: None
        """
        if not repo_name:
            repo_name = self.REPO_NAME
        if not schema_path:
            schema_path = self.SCHEMA_PATH
        self.schema_fname = self.testbase_dir + repo_name + '-schema.json'
        schema_content = self.fetch_file(repo_name, schema_path, branch)
        schema_final = self.extract_schema(schema_content)
        with open(self.schema_fname, 'w') as schema_file:
            json.dump(schema_final, schema_file)


    ######################################################
    #      Stubs for collection-specific methods
    ######################################################
    def pre_request_actions(self, request):
        return (), request

    def post_request_actions(self, *_):
        return ()

    def custom_store_steps(self, *_, **__):
        return ()

    def folder_suite_setup(self, *_, **__):
        """
        Stub for folder suite setup
        :param _: ignored
        :param __: ignored
        :return: Empty values for setup lines, variables, resources
        """
        return (), (), ()

    def set_common_suite_vars(self):
        self.variables.extend(ROBOT_VARIABLE(name, obj) for name, obj in
                            ((C.CREATED_OBJS, {}), (C.DELETED_OBJS, [])))
        self.variables.append(ROBOT_VARIABLE(C.DESCRIPTION, self.suite_descr))

    def update_request_data(self, request, name):
        """
        Update request dictionary with processed components to simplify further processing
        Empty values send to Postman script are replaced by Postman representation of None
        :param request:
        :param name:
        :return:
        """
        body = request.pop(C.BODY, '')
        name = tokenizer(name)

        # Get and update request body
        req_body_tuple = ()
        if request[C.METHOD] in (C.GET, C.DELETE):
            # Some request should not have body
            body = ROBOT_TYPED(None)
        elif isinstance(body, dict):
            body = body.get(C.RAW, '').replace("'", '"')
            body = body * (body != '{}')
            if body:
                if request[C.METHOD] != C.PATCH:
                    # Separate body content - stored in Python file -
                    # and replace content with variable name
                    req_body_tuple = ROBOT_VARIABLE(name + '_JSON', json.loads(body))
                    body = RVAR(req_body_tuple.name)
            else:
                body = ROBOT_TYPED(None)

        # Update headers - don't send default
        headers = {h[C.KEY]: h[C.VALUE] for h in request[C.HEADER]}
        if len(headers) == 0 or list(headers.values()) == list(DEFAULT_HEADER.values()):
            headers = ROBOT_TYPED(None)

        # Clean URL and - if exists - replace common path prefix
        # Trailing slashes only if URL is the same as common prefix
        url = request[C.URL].rstrip('/')
        if self.common_url_prefix:
            shared_url_var = RVAR(C.URL_PREFIX)
            url  = url.replace(self.common_url_prefix, shared_url_var)
            if url == shared_url_var:
                url += '/'

        request.update(((C.NAME, name), (C.BODY, body), (C.HEADERS, headers),
                        (C.URL, url), (C.EXTERNAL_VAR, req_body_tuple)))
        return request

    #################################################################
    #    Methods to create flows to pick objects in Robot run-time
    #################################################################
    def random_existing_id(self, request, obj_name, source_dict=C.CREATED_OBJS, field_name=None, id_pos=-1):
        """
        Provides step for picking random object id from dictionary of created objects
        :param request: rest request
        :param obj_name: used to create variable name
        :param source_dict: source to pick from
        :param id_pos: position of ID component in URL string
        :return: command for ID chosing, request with updated URL
        """
        url = request[C.URL]
        url_components = url.split('/')
        id_name = obj_name.replace(' ', '_') + '_id'
        if field_name is None:
            id_selection_step = '{} =  {}  {}  {}'.format(
                RVAR(id_name), KW.SELECT_RANDOM_CREATED_OBJECT, RVAR(source_dict), obj_name)
        else:
            id_selection_step = '{} =  {}  {}  {}  {}'.format(
                RVAR(id_name), KW.FIELD_FROM_RANDOM_OBJECT, RVAR(source_dict), obj_name, field_name)
        url_components[id_pos] = RVAR(id_name)
        request[C.URL] = '/'.join(url_components)
        return id_selection_step, request, id_name

    def random_existing_path(self, source_dict, url_components, unresolved=1):
        """
        Pick path components from "mapping" dictionaries that group stored object IDs
        by values appearing in object. Unresolved components must have same names
        as keys in stored objects (keys may be at at different depths)
        :param source_dict: mapping dictionary
        :param url_components: URL components that contain unresolved paths
        :param unresolved: number of unresolved components
        :return: step that sets variables, resolved URL fragment
        """
        variable_names = [v.strip('{}') for v in url_components[-unresolved:]]
        assign_step = self.assign_mapping_step(source_dict, *variable_names)
        url_fragment = '/'.join(url_components[:-unresolved] + list(RVAR(v) for v in variable_names))
        verify_step = self.verify_mapping_step(source_dict, *variable_names)
        return assign_step, verify_step, url_fragment

    ################################################################
    #       Factories for typical steps
    ################################################################
    def store_object_step(self, obj_descr,
                          obj_name=C.REST_RESPONSE, dict_name=C.CREATED_OBJS,
                          id_field=None, lookup_id=True):
        if id_field is None:
            id_field = self.ID_FIELD
        return '{2} =    {0}  {4}  {1}  {2}  {3}  {5}'.format(KW.STORE_CREATED_OBJECT,
            RVAR(obj_name), RVAR(dict_name), id_field, QUOTE(obj_descr), ROBOT_TYPED(lookup_id))

    def store_mapping_step(self, obj_descr,
                          obj_name=C.REST_RESPONSE, dict_name=C.CREATED_OBJS,
                          component_keys=(), id_field=None):
        if id_field is None:
            id_field = self.ID_FIELD
        return '{2} =    {0}  {4}  {1}  {2}  {3}  {5}'.format(KW.STORE_CREATED_OBJECT,
            RVAR(obj_name), RVAR(dict_name), id_field, QUOTE(obj_descr), '  '.join(component_keys))

    def validate_subdictionary_step(self, source_dict, obj_description,
                                    validated_dict=C.REST_RESPONSE,
                                    path_to_subdict=None, exclude_xpaths=()):
        if exclude_xpaths == ():
            exclude_xpaths = self.EXCLUDED_XPATHS
        mandatory_fields = (KW.OBJECT_COMPONENTS_VALID, QUOTE(obj_description),
                            RVAR(validated_dict), RVAR(source_dict), ROBOT_TYPED(path_to_subdict))
        return '  '.join(mandatory_fields + exclude_xpaths)

    def validate_field_step(self, field, value2validate, obj_description,
                            validated_dict=C.REST_RESPONSE, value_as_var=True):
        if value_as_var:
            value2validate = RVAR(value2validate)
        return '  '.join((KW.OBJECT_FIELD_VALID, QUOTE(obj_description),
                         RVAR(validated_dict), field, value2validate))

    def validate_received_obj_step(self, obj_description,
                                   obj_name=C.REST_RESPONSE, source_dict=C.CREATED_OBJS,
                                   exclude_xpaths=(), id_field=None):
        if exclude_xpaths == ():
            exclude_xpaths = self.EXCLUDED_XPATHS
        if id_field is None:
            id_field = self.ID_FIELD
        mandatory_fields = (KW.VALIDATE_RESPONSE_OBJECT_AGAINST_STORED, QUOTE(obj_description),
                            RVAR(obj_name), RVAR(source_dict), id_field)
        return '  '.join( mandatory_fields + exclude_xpaths)

    def validate_bulk_query_step(self,  obj_description, received_obj=C.REST_RESPONSE,
                                 source_dict=C.CREATED_OBJS, deleted_list=C.DELETED_OBJS,
                                 after_get=True, exclude_xpaths=()):
        if exclude_xpaths == ():
            exclude_xpaths = self.EXCLUDED_XPATHS
        validation_kw = after_get and KW.VALIDATE_CREATED_OBJECTS_IN_BULK_QUERY \
                        or KW.VALIDATE_BULK_DELETED_OBJECTS
        mandatory_fields = (validation_kw, QUOTE(obj_description)) + \
                            tuple(RVAR(v) for v in (received_obj, source_dict, deleted_list))
        return '  '.join(mandatory_fields + exclude_xpaths)

    def validate_object_list_step(self,  obj_description, received_obj=C.REST_RESPONSE,
                                  verifification_source=C.CREATED_OBJS, id_field=None):
        return '  '.join((KW.VALIDATE_OBJECT_LIST_IN_RESPONSE, QUOTE(obj_description),
                            RVAR(received_obj), RVAR(verifification_source), ROBOT_TYPED(id_field)))

    def remove_object_step(self, key, source_dict=C.CREATED_OBJS, deleted_list=C.DELETED_OBJS):
        return '{1}    {3} =   {0}  {1}  {2}  {3}'.format(
            KW.REMOVE_DELETED_OBJECT, *(RVAR(v) for v in (source_dict, key, deleted_list)))

    def update_expected_val_step(self, id_value, update_value, source_dict=C.CREATED_OBJS,
                                 var_name=C.EXPECTED_VALUE, id_as_var=True):
        if id_as_var:
            id_value = RVAR(id_value)
        return '{} =   {}'.format(RVAR(var_name),
            '  '.join((KW.UPDATE_EXPECTED_RECORD, RVAR(source_dict), id_value, RVAR(update_value))))

    def verify_mapping_step(self, source_dict, *variables, received_obj=C.REST_RESPONSE, id_field=None):
        if id_field is None:
            id_field = self.ID_FIELD
        return '  '.join((KW.VALIDATE_OBJECT_MAPPINGS_IN_RESPONSE, RVAR(received_obj), RVAR(source_dict), id_field) +
                         tuple(RVAR(v) for v in variables))

    def assign_mapping_step(self, source_dict, *variables):
        return '{} =    {}  {}'.format('    '.join(RVAR(v) for v in variables),
                                       KW.SET_RANDOM_URL_COMPONENTS, RVAR(source_dict))

    ################################################################
    #           Simple methods implementation -
    # should be overloaded if more complex approach required
    ################################################################
    def post_method(self, request, obj_description,
                    exclude_xpaths=(), verified_xpath=None, **validation_args):
        """
        Generic posting method
        :param request: request object
        :param obj_description: name - for logging purposes
        :param exclude_paths: xpaths in received object that should be excluded from verification
        :param path: path - xpath to verified subdictionary in original object
        :return: list of steps, list of variables
        """
        steps = [self.make_request_record(request)]
        body_var = request[C.EXTERNAL_VAR]
        obj_type = obj_description
        obj_description = 'POSTed ' + obj_description
        validate_as_is = validation_args.get(C.AS_LIST, False)
        if validate_as_is:
            steps.append(self.validate_object_list_step(obj_description,
                                                        verifification_source=body_var.name))
        else:
            steps.append(self.validate_subdictionary_step(body_var.name, obj_description,
                                                      path_to_subdict=verified_xpath,
                                                      exclude_xpaths=exclude_xpaths))
        lookup_id = validation_args.get(C.LOOKUP_ID, True)
        if lookup_id:
            kwargs = {}
        else:
            kwargs = {'id_field': obj_type, C.LOOKUP_ID: False}
        steps.append(self.store_object_step(obj_description, **kwargs))
        return steps, [body_var]

    def put_method(self, request, obj_description,
                   exclude_xpaths=(), pick_id=True, id_pos=-1, **_):
        """
        Generic put method
        :param request: request object
        :param obj_description: name - for logging purposes
        :param exclude_paths: xpaths in received object that should be excluded from verification
        :param pick_id: select random ID on True
        :param id_pos: position of ID component in URL string
        :return: list of steps, list of variables
        """
        expected_rc = ('negative' in request[C.NAME].lower()) and 400 or 200
        steps = []
        id_var = ''
        if expected_rc == 200:
            if pick_id:
                set_id_step, request, id_var = self.random_existing_id(request, obj_description, id_pos=id_pos)
                steps.append(set_id_step)
            else:
                id_var = request[C.URL].split('/')[id_pos]
        steps.append(self.make_request_record(request, expected_rc=expected_rc))
        body_var = request[C.EXTERNAL_VAR]
        if expected_rc == 200:
            obj_description = 'Updated ' + obj_description
            steps.append(self.validate_field_step(self.ID_FIELD, id_var,
                          obj_description + ' ID', value_as_var=pick_id))
            value2validate = body_var.name
            steps.append(self.update_expected_val_step(id_var, value2validate, id_as_var=pick_id))
            value2validate = C.EXPECTED_VALUE
            steps.append(self.validate_subdictionary_step(value2validate, obj_description,
                                                          exclude_xpaths=exclude_xpaths))
            steps.append(self.store_object_step(obj_description))
        variables = (body_var, ) if body_var else ()
        return steps, variables


    def get_method(self, request, obj_description,
                   exclude_xpaths=(), pick_id=True, id_pos=-1, **validation_args):
        """
        Generic GET request
        :param request: request object
        :param obj_description: name - for logging purposes
        :param exclude_paths: xpaths in received object that should be excluded from verification
        :param pick_id: on True, add step to find random existing object
        :param id_pos: relative position of ID in URL
        :return: list of steps, list of variables
        """
        expect_success = 'negative' not in request[C.NAME].lower()
        expected_rc = expect_success and 200 or 404
        steps = []
        id_var = ''
        if expect_success and pick_id and 'id' in request[C.URL].split('/')[id_pos].lower():
            set_id_step, request, id_var = \
                self.random_existing_id(request, obj_description, id_pos=id_pos)
            steps.append(set_id_step)
        request_step = self.make_request_record(request, expected_rc=expected_rc)
        if expect_success and validation_args.get('validation', True):
            # Occasionally, GET validations fails on race condition - add loop with re-tries
            steps.append(":FOR    ${{attempt}}    IN RANGE    1    {}".format(self.get_retries +1),)
            steps.append('\    ' + request_step)
            obj_type = obj_description
            obj_description = 'Queried '+ obj_description
            if id_var:
                validation_step = self.validate_received_obj_step(obj_description)
            elif validation_args.get(C.AS_LIST, False):
                validation_step = self.validate_object_list_step(obj_description, id_field=obj_type)
            else:
                validation_step = self.validate_bulk_query_step(obj_description)
            steps.append("\    ${status}    ${ret_value} =    Run Keyword And Ignore Error  " +
                         validation_step)
            steps.append("\    Exit For Loop If    '${status}' == 'PASS'")
            steps.append("\    Run Keyword If    ${{attempt}} == {0}    Fail    "
                         "'GET with verifications after {0} retries; Error - ${{ret_value}}'".format(self.get_retries))
            steps.append("\    Sleep  1s    'Verification failed - retry GET, attempt ${attempt}'")
        else:
            steps.append(request_step)
        return steps, ()

    def delete_method(self, request, obj_description,
                      exclude_xpaths=(), pick_id=True, id_pos=-1, **_):
        """
        Generic DELETE request
        :param request: request object
        :param obj_description: name - for logging purposes
        :param exclude_paths: xpaths in received object that should be excluded from verification
        :param pick_id: on True, add step to find random existing object
        :param id_pos: relative position of ID in URL
        :return: list of steps, list of variables
        """
        url = request[C.URL]
        expect_success = 'negative' not in request[C.NAME].lower()
        expected_rc = expect_success and 200 or 404
        steps = []
        if expect_success and 'id' in url.rpartition('/')[id_pos].lower():
            set_id_step, request, id_var = \
                self.random_existing_id(request, obj_description, id_pos=id_pos)
            steps.append(set_id_step)
        else:
            id_var = request[C.URL].split('/')[id_pos]
        steps.append(self.make_request_record(request, expected_rc=expected_rc))
        if expected_rc == 200:
            steps.append(self.validate_received_obj_step('Deleted by ID ' + obj_description))
            steps.append(self.remove_object_step(id_var))
        return steps, ()

    def patch_method(self, request, obj_description, **_):
        id_var = obj_description + '_IDs'
        body = json.loads(request[C.BODY])
        ids_selection_step = '{} =    {}  {}  {}  {}'.format(
            RVAR(id_var), KW.SELECT_RANDOM_CREATED_OBJECT, RVAR(C.CREATED_OBJS),
            obj_description, ROBOT_TYPED(len(body)))
        request[C.BODY] = RVAR(id_var)
        steps = [ids_selection_step,
                 self.make_request_record(request),
                 self.validate_bulk_query_step(obj_description, after_get=False),
                 ':FOR    ${{obj_id}}    IN    @{{{}}}'.format(id_var),
                 '\    ' + self.remove_object_step('obj_id')]
        return steps, ()


    def make_request_params(self, request, use_kw_args=True):
        """
        Build parameter string for REST request
        :param request: request structure - on string, build template
        :param name: request name
        :param bod¡y_name: use custom name for body variable -"name" on default
        :param use_kw_args: create request string with KW args or positional only(for template)
        :return: string of request arguments, (body var.name, body var. value) tuple
        """
        if isinstance(request, str):
            # Template
            request = {f: RVAR(f) for f in self.REQ_FIELDS}
        return self.REQ_FMT_POS.format(**request).rstrip()


    def make_request_record(self, request,
                            response_name=C.REST_RESPONSE, expected_rc=200):
        """
        Create request step for TC
        :param request: request structure - on string, build template
        :param response_name: name for response variable
        :return: string of request step, (body var.name, body var. value) tuple
        """
        request_params = self.make_request_params(request)
        # Request keyword depends on context - for template, it's method from helper object
        # for test - global request keyword
        if 'Base' not in self.__class__.__name__:
            # Real test case
            req_keyword = KW.REST_REQUEST_WITH_STATUS_VALIDATION
            request_params += '  expected_rc=' + ROBOT_TYPED(expected_rc)
        else:
            # Template
            req_keyword = KW.SEND_REQUEST
            request_params = request_params.rpartition('  ')[0]
        request_string = '${{{name}}} =    {req}   {params}'.format(
            name=response_name, req=req_keyword, params=request_params)
        return request_string

    def prepare_suite(self):
        pass

    def get_object_name(self, _):
        """
        Generic implementation - override for objects with differing criteria
        :param _:
        :return:
        """
        return self.common_url_prefix.rpartition('/')[-1]

    def create_file_testsuite(self, suite_name, request_items, *resources):
        """
        Filters items from collection to split by contained requests
        Items with request are used to create file test suite.
        Items without requests are returned for further processing as test suites
        :param suite_name: Suite name
        :param items: list of collection sub-objects
        :param resources: Resources for this lebel
        :return: items to be re-processed as suites
        """
        request_tuples = [(i[C.REQUEST], i[C.NAME]) for i in request_items]
        self.common_url_prefix = common_url_prefix(*[r[0][C.URL] for r in request_tuples])
        request_list = [self.update_request_data(*r) for r in request_tuples]

        self.set_common_suite_vars()
        self.resources.extend(resources)
        self.prepare_suite()
        for request in request_list:
            obj_name = self.get_object_name(request)
            self.test_cases.append(request[C.NAME])
            num_tc_lines = len(self.test_cases)
            pre_steps, request = self.pre_request_actions(request)
            self.test_cases.extend(pre_steps)
            req_steps, req_variables = getattr(self, request[C.METHOD].lower() + '_method') \
                (request, obj_name)
            self.test_cases.extend(req_steps)
            post_steps = self.post_request_actions(request, req_steps)
            self.test_cases.extend(post_steps)
            self.variables.extend(req_variables)
            if len(self.test_cases) == num_tc_lines:
                # No actions for TC - purge from TC lines
                del self.test_cases[-1]
            else:
                self.test_cases.append('')
        self.write_testsuite_file(suite_name)


    def write_testsuite_file(self, suite_name):
        """
        Write file test suite by components
        :param suite_name: suite name
        :param resources: resource list
        :param libraries: library list
        :param suite_setup: suite setup list
        :param keywords: keywords definitions list
        :param test_cases: test cases list
        :param variables: variables list
        :param documents: suite documents list
        :return: None
        """
        def _write_tables(rows, *row_titles):
            """
            Wrap creations of tables in test suite file
            :param rows: list values for rows
            :param row_titles: titles for rows (filled to number of rows, if required)
            :return: None
            """
            if not rows:
                return
            table = list(zip(row_titles[:-1] + (row_titles[-1], ) * (len(rows) + 1 - len(row_titles)),
                             rows))
            robot_file.write(align_rows(table))
            robot_file.write('\n')

        def write_section(section_lines, section_header, tag_line=''):
            if not section_lines:
                return
            robot_file.write('\n\n\n' + section_header)
            # Empty previous string indicates new TC start - remove indent
            section_lines.insert(0, '')
            if tag_line:
                # Tags are added to each distinct sub-section
                tag_indexes = [idx + 1 for idx, tc in enumerate(section_lines[:-1])
                               if '' not in (tc, section_lines[idx + 1]) and section_lines[idx - 1] == '']
                for tag_index in reversed(tag_indexes):
                    section_lines.insert(tag_index, TAG_LINE)
            robot_file.write('\n'.join('    ' * ('' not in (section_lines[idx - 1], tc)) + tc
                                       for idx, tc in enumerate(section_lines)))

        full_suite_name = self.testbase_dir + suite_name

        if self.variables and self.test_cases:
            # Test case variables are managed in Python files
            variables_name = full_suite_name + '.py'
            with open(variables_name, 'w') as variable_file:
                if self.common_url_prefix:
                    variable_file.write('{} = "{}"\n\n'.format(C.URL_PREFIX, self.common_url_prefix))
                    self.common_url_prefix = ''
                val_convert = lambda v: isinstance(v, (dict, list, tuple)) and pprint.pformat(v) or repr(v)
                for var_name, var_value in self.variables:
                    variable_file.write('{} = {}\n\n'.format(var_name, val_convert(var_value)))
            self.resources.append(variables_name.replace(C.PATH, '${BASE_DIR}'))
            resources_title = ('Resource', 'Variables')
            self.variables.clear()
        else:
            resources_title = ('Resource', )

        with open(full_suite_name + '.robot', 'w') as robot_file:
            robot_file.write(TC_SECTIONS.SETTINGS + '\n')
            _write_tables(self.documents, 'Documentation', '...')
            _write_tables(self.libraries, 'Library')
            _write_tables(self.resources, *resources_title)
            robot_file.write('\n'.join(self.suite_setup))
            write_section(self.keywords, TC_SECTIONS.KEYWORDS)
            if self.test_cases:
                if TC_SECTIONS.TEST_CASES not in str(self.test_cases):
                    write_section(self.test_cases, TC_SECTIONS.TEST_CASES, TAG_LINE)
                else:
                    # Template TC
                    robot_file.write('\n\n\n' + '\n'.join(self.test_cases))
            if self.variables:
                robot_file.write('\n\n\n' + TC_SECTIONS.VARIABLES + '\n')
                variables_table = [(RVAR(vname), value) for vname, value in self.variables]
                robot_file.write(align_rows(variables_table))
            robot_file.write('\n\n')
            self.clean_suite_components()


    def skip_test(self, description):
        """
        Check test skip condition
        :param description: test description
        :return: skip condition
        """
        return 'skip' in description.lower()
