"""
Contains class for Config collection
"""

from collection_utils import collection_base
from test_consts import C, RVAR, QUOTE

class Config(collection_base.CollectionBase):
    """
    Flow test cases - re-uses base class APIs
    """
    def get_object_name(self, request):
        return request[C.URL].rpartition('/')[-1]

    def get_method(self, request, obj_description):
        validation = obj_description != 'avatarConfig'

        steps, _ = super(Config, self).get_method(request, obj_description, validation=validation)
        if not validation:
            steps.append('  '.join(('Dictionaries Should Be Equal', RVAR(C.REST_RESPONSE), RVAR(C.CREATED_OBJS),
                            QUOTE('All avatar config objects'))))
        return steps, ()

