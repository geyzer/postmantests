"""
Contains class for Octopus collection
"""

from test_consts import C
from robot_utils import KW, TC_SECTIONS
from reckon_utils import align_rows
from collection_utils import collection_base

class Octopus(collection_base.CollectionBase):
    """
    Creates test suites for testing scrapers' REST APIs
    """
    REPO_NAME = 'octopus'
    SCHEMA_PATH = 'output_schema.json'

    def create_specific_testsuite(self, suite_name, request_list, *resources):
        """
        Create Octopus - specific test suite - login avatar and run scrapers
        Octopus suites are build as Data Driven Tests - templates
        :param suite_name: name of scraper suite
        :param request_tuples: (name, request) tuples
        :param resources: resources
        :return: None
        """
        login_reqs = [i for i in request_list if 'login' in i[C.URL]]
        if not login_reqs:
            # Login request required!
            return
        suite_setup = ('Suite Setup      Avatar Login',
                       'Test Template    Scrape Request')
        login_req = login_reqs[0]
        keywords = ['Avatar Login', self.REQ_ARGS,
                    self.make_request_record(login_req),
                    '']
        request_list.remove(login_reqs[0])
        variables = login_req[C.EXTERNAL_VAR]

        sample_request = request_list[0]
        keywords.extend(('Scrape Request', self.REQ_ARGS))
        keywords.extend(self.pre_request_actions(sample_request)[0])
        keywords.append(self.make_request_record('template'))
        keywords.extend(self.post_request_actions(sample_request)[0])
        test_cases = []
        for req_data in request_list:
            request_params = self.make_request_params(req_data, use_kw_args=False)
            test_cases.append([req_data[C.NAME]] + request_params.split())
            variables.append(req_data[C.EXTERNAL_VAR])
        test_cases.insert(0, [TC_SECTIONS.TEST_CASES] + [f[0].title() for f in self.REQ_FIELDS])
        test_cases = align_rows(test_cases)
        self.write_testsuite_file(suite_name, resources=resources, suite_setup=suite_setup, keywords=keywords,
                                  test_cases=test_cases, variables=variables)


    def post_request_actions(self, request):
        """
        Post-request actions - check fields in response, validate schema
        :param request: request structure
        :return: List of TC steps, empty Postman variables list, emppty intest variables dict
        """
        return ('{}    ${{response}}'.format(KW.COMPARE_FIELDS),
                '{}    ${{response}}  {}'.format(KW.VALIDATE_RESPONSE, self.schema_fname)), \
                (), {}
