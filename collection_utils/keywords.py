
from test_consts import C, OBJECTS_CLEANUP
from collection_utils import collection_base

class Keywords(collection_base.CollectionBase):
    ID_FIELD = 'id'

    def prepare_suite(self):
        super(Keywords, self).prepare_suite()
        self.suite_setup.append(OBJECTS_CLEANUP)

    def pre_request_actions(self, request):
        if 'case_id' in request[C.URL]:
            set_id_step, request, _ = \
                self.random_existing_id(request, self.get_object_name(request), field_name='case_id')
            return (set_id_step, ), request
        return (), request

    def put_method(self, request, obj_description):
        return super(Keywords, self).put_method(request, obj_description,
                                                pick_id='case_id' not in request[C.URL])

    def delete_method(self, request, obj_description):
        return super(Keywords, self).delete_method(request, obj_description,
                                                pick_id='case_id' not in request[C.URL])

