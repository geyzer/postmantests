#!/usr/bin/env python3
from robot.api.deco import keyword
import robot.utils.asserts as RAsserts
import requests
import json
import random
import pprint

# from logutils import logutils_factory
from test_consts import C, KW, DEFAULT_HEADER, QUOTE
from robot_utils import logger_obj
from reckon_utils import remove_multiple_json_xpaths, find_values_for_key_list, json_within_string

xpaths_msg = lambda xpaths: 'excluding XPATHs: {}'.format(','.join(QUOTE(p) for p in xpaths)) \
                            if xpaths else ''

class TestHelper(object):
    ROBOT_LIBRARY_SCOPE = 'GLOBAL'
    ROBOT_CONTINUE_ON_FAILURE = True

    def __init__(self, test_output):
        self.__dump_folder = test_output +'/'

    @keyword(KW.REST_REQUEST_WITH_STATUS_VALIDATION, tags=[C.REST])
    def rest_request(self, method, url, body=None, headers=None, expected_rc=200):
        response = self.send_request(method, url, body, headers)
        response_json = json_within_string(response.text)
        response_text = pprint.pformat(response_json) if response_json else response.text
        RAsserts.assert_equal(response.status_code, expected_rc,
                              'Failed with RC={} (expected {}): {}\n{}\n{}\n{}'
                              .format(response.status_code, expected_rc, response.reason,
                                      C.RESPONSE_CONTENT, '-' * len(C.RESPONSE_CONTENT), response_text),
                              values=False)
        if expected_rc in (200, 201):
            RAsserts.assert_(response_json != {}, 'Unexpected response content: "{}"'.format(response.text))
            return response_json
        return response


    @keyword(KW.SEND_REQUEST)
    def send_request(self, method, url, body='', headers=''):
        # headers = (isinstance(headers, str) and headers != '') and json.loads(headers) or DEFAULT_HEADER
        request_kwargs = {C.HEADERS: headers or DEFAULT_HEADER}
        if body:
            if isinstance(body, str):
                body = json.loads(body)
            request_kwargs[C.JSON] = body
        return requests.request(method, url, **request_kwargs)


    @keyword(KW.VALIDATE_RESPONSE_OBJECT_AGAINST_STORED, tags=[C.VALIDATION])
    def validate_response_object_against_stored(self, obj_description, received_obj, stored_dict,
                                                id_field, *exclude_xpaths):
        logger_obj.info('Received {} object must be same as stored' + xpaths_msg(exclude_xpaths))
        id_value = received_obj.get(id_field)
        RAsserts.assert_not_none(id_value, 'Received object {} does not contain field "{}"'
                                 .format(obj_description, id_field))
        updated_received_object = remove_multiple_json_xpaths(received_obj, *exclude_xpaths)
        updated_stored_obj = remove_multiple_json_xpaths(stored_dict[id_value], *exclude_xpaths)
        RAsserts.assert_equal(updated_received_object, updated_stored_obj)


    @keyword(KW.OBJECT_COMPONENTS_VALID, tags=[C.VALIDATION])
    def object_components_valid(self, obj_description, obj_to_validate, valid_obj,
                                path_to_subdict=None, *exclude_xpaths):
        logger_obj.info('Validate object components for ' + obj_description +
                        ' at path "{}"'.format(path_to_subdict) * (not path_to_subdict is None))
        if not path_to_subdict is None:
            for leg in path_to_subdict.split('/'):
                valid_obj = valid_obj.get(leg)
                RAsserts.assert_not_none(valid_obj, 'Path "{}" - no entry at "{}"'.format(path_to_subdict, leg))
        updated_obj2validate = remove_multiple_json_xpaths(obj_to_validate, *exclude_xpaths)
        updated_valid_obj = remove_multiple_json_xpaths(valid_obj, *exclude_xpaths)
        for field, value2validate in updated_valid_obj.items():
            self.validate_object_field(obj_description, updated_obj2validate, field, value2validate, log_step=False)


    @keyword(KW.OBJECT_FIELD_VALID, tags=[C.VALIDATION])
    def validate_object_field(self, obj_description, obj_to_validate, field, value2validate, log_step=True):
        full_validation = not isinstance(value2validate, bool)  # On True, just chek field presense
        if log_step:
            if full_validation:
                msg = 'Validate {}: expect "{{{}: {}}}"'.format(obj_description, field, value2validate)
            else:
                msg = 'Validate {} field present in {}'.format(field, )
            logger_obj.info(msg)
        field_value = obj_to_validate.get(field)
        RAsserts.assert_not_none(field_value,
                                 '{}: Field "{}" missing in target'.format(obj_description, field))

        RAsserts.assert_equal(value2validate, field_value,
                              'Validate {}, field "{}"'.format(obj_description, field))


    @keyword(KW.VALIDATE_CREATED_OBJECTS_IN_BULK_QUERY, tags=[C.VALIDATION])
    def validate_created_objects_in_bulk_query(self, obj_description, received_obj, stored_dict, deleted_objs=(),
                                               *exclude_xpaths):

        logger_obj.info('Verify all existing {} objects in bulk query'.format(obj_description))

        received_updated = [remove_multiple_json_xpaths(obj, *exclude_xpaths) for obj in received_obj]
        for stored_obj in stored_dict.values():
            obj_2_check = remove_multiple_json_xpaths(stored_obj, *exclude_xpaths)
            RAsserts.assert_(obj_2_check in received_updated, 'Missing in received {}: {}'
                             .format(obj_description, pprint.pformat(stored_obj)))

        if deleted_objs:
            logger_obj.info('Verify deleted {} objects not present in bulk query results'.format(obj_description))
        for deleted_obj in deleted_objs:
            deleted_updated = remove_multiple_json_xpaths(deleted_obj, *exclude_xpaths)
            RAsserts.assert_(deleted_updated not in received_updated,
                             'Deleted {} object in bulk query!\n{}'.format(obj_description, pprint.pformat(deleted_obj)))


    @keyword(KW.VALIDATE_BULK_DELETED_OBJECTS, tags=[C.VALIDATION])
    def validate_bulk_deleted_objects(self, obj_description, received_obj, stored_dict, deleted_objs=(),
                                               *exclude_xpaths):
        logger_obj.info('Verify bulk deleted "{}" objects - in created objects and not in deleted list'
                        .format(obj_description))

        stored_updated = [remove_multiple_json_xpaths(obj, *exclude_xpaths) for obj in stored_dict.values()]
        deleted_updated = [remove_multiple_json_xpaths(obj, *exclude_xpaths) for obj in deleted_objs]
        for received_rec in received_obj:
            obj_2_check = remove_multiple_json_xpaths(received_rec, *exclude_xpaths)
            RAsserts.assert_(obj_2_check in stored_updated, 'Deleted object missing in stored {}: {}'
                             .format(obj_description, pprint.pformat(received_rec)))
            RAsserts.assert_(obj_2_check not in deleted_updated, 'Bulk deleted object in deleted list: {}'
                             .format(obj_description, pprint.pformat(received_rec)))


    @keyword(KW.VALIDATE_OBJECT_LIST_IN_RESPONSE, tags=[C.VALIDATION])
    def validate_bulk_list_in_response(self, obj_description, received_obj, verification_source, id_field=None):
        if not id_field is None:
            logger_obj.info('Verifed stored {} object list'.format(obj_description))
            verification_source = verification_source.get(obj_description)
            RAsserts.assert_not_none(verification_source, 'No known objects of type "{}"'.format(obj_description))
        else:
            logger_obj.info('Verify bulk creation response')
        for expected_value in verification_source:
            RAsserts.assert_(expected_value in received_obj, 'Missing in received {}: {}'
                                .format(obj_description, pprint.pformat(expected_value)))

    @keyword(KW.VALIDATE_OBJECT_MAPPINGS_IN_RESPONSE, tags=[C.VALIDATION])
    def verify_object_ids_in_response(self, received_obj, source_dict, id_field, *mapping_key):
        received_ids = set(obj[id_field] for obj in received_obj)
        expected_ids = source_dict[mapping_key]
        RAsserts.assert_(expected_ids.issubset(received_ids), 'Missing {} object IDs: '.format(
            mapping_key, ', '.join(expected_ids - received_ids)))

    @keyword(KW.SET_RANDOM_URL_COMPONENTS, tags=[C.NONTEST])
    def set_random_url_components(self, mapping_dict):
        keys = list(mapping_dict.keys())
        selected_key = random.choice(keys)
        return selected_key


    @keyword(KW.SELECT_RANDOM_CREATED_OBJECT, tags=[C.NONTEST])
    def select_random_created_object(self, obj_dict, obj_description=None, num_objs=1):
        if not obj_description:
            logger_obj.info('Use random {} object for the next test step'.format(obj_description))
        created_keys = list(obj_dict.keys())
        RAsserts.assert_(len(created_keys) > 0, 'Dictionary of created {} is empty!'.format(obj_description))
        if num_objs == 1:
            return random.choice(created_keys)
        RAsserts.assert_(len(obj_dict) >= num_objs, '{} objects - not enough for {} values'
                         .format(len(obj_dict), num_objs))
        choices = set()
        while len(choices) < num_objs:
            choices.add(random.choice(created_keys))
        return list(choices)


    @keyword(KW.FIELD_FROM_RANDOM_OBJECT, tags=[C.NONTEST])
    def field_value_from_random_object(self, obj_dict, obj_description, field_key):
        logger_obj.info('Use {} field value from random object {} for the next test step'
                        .format(field_key, obj_description))
        object_key = self.select_random_created_object(obj_dict, obj_description)
        field_value = obj_dict[object_key].get(field_key)
        RAsserts.assert_not_none('Object {} does not contain field {}'.format(obj_description, field_key))
        return field_value


    @keyword(KW.SELECT_SOURCE_DICT, tags=[C.NONTEST])
    def select_source_dictionary(self, key, *dictionaries, msg=''):
        for dictionary in dictionaries:
            if key in dictionary:
                return dictionary
        assert False, 'No key "{}" in any of source dicts{}{}'.format(key, '; ' * (msg != ''), msg)

    @keyword(KW.REMOVE_DELETED_OBJECT, tags=[C.NONTEST])
    def remove_deleted_object(self, dict_obj, obj_key, deleted_list):
        deleted_list.append(dict_obj.pop(obj_key))
        return dict_obj, deleted_list

    @keyword(KW.UPDATE_EXPECTED_RECORD, tags=[C.NONTEST])
    def update_expected_record(self, source_dict, key, update_value):
        record2update = source_dict[key]
        record2update.update(update_value)
        return record2update


    @keyword(KW.STORE_CREATED_OBJECT, tags=[C.NONTEST])
    def store_created_object(self, obj_description, obj2store, dict_obj, id_field, lookup_id=True):
        """
        Store created object for future reference
        :param obj2store: object to store
        :param id_field: object identification field
        :param dict_obj: dictionary to store object - or its ID - in
        :param obj_description: description of object
        :param lookup_id: True for objects that contain "id_field"
        :return: updated dictionary object
        """
        if lookup_id:
            if isinstance(obj2store, dict):  # Just ease object update flow
                obj2store = (obj2store, )
            for value in obj2store:
                # Bulk post requests may contain bulk responses
                id_value = value[id_field]
                RAsserts.assert_not_none(id_value, 'Object {} does not contain ID field "{}"'
                                         .format(obj_description, id_field))
                dict_obj[id_value] = value
        else:
            # Special case - store received object "as is"  against the provided id_field as key
            dict_obj[id_field] = obj2store
        return dict_obj

    @keyword(KW.STORE_PROPERTY_MAPPINGS, tags=[C.NONTEST])
    def store_object_property_mappings(self, obj_description, obj2store,
                                       dict_obj, id_field, *key_components):
        """
        Store mappings from property values to object IDs
        Key Components define object fields - to use as mapping source
        :param obj2store: object to store mapping for
        :param id_field: object identification field
        :param dict_obj: dictionary to store sets of IDs in
        :param obj_description: description of object
        :param key_components: keys of subdictionaries present in the object to build storage key
        :return: updated dictionary object
        """
        id_value = obj2store[id_field]
        RAsserts.assert_not_none(id_value, 'Object {} does not contain ID field "{}"'
                                 .format(obj_description, id_field))
        # ID is stored in sets for easy management;
        # single-element keys are converted from tuple to scalar form
        key_conv = (lambda v: v[0]) if len(key_components) == 1 else (lambda v: v)
        keys2map = [key_conv(k) for k in find_values_for_key_list(obj2store, *key_components)]
        RAsserts.assert_(keys2map != [],
                         'Object to store doesn\'t contain components with all of keys "{}"'
                         .format(', '.join(key_components)))
        # Map new/updated object
        for key in keys2map:
            dict_obj.setdefault(key, set()).add(id_value)

        # CLean old irrelevant mappings
        for keys2unmap in set(dict_obj) - set(keys2map):
            dict_obj[keys2unmap].discard(id_value)
            if not dict_obj[keys2unmap]:
                dict_obj.pop(keys2unmap)
        return dict_obj


    @keyword(KW.CLEAN_TEST_CREATED_OBJECTS)
    def clean_test_created_objects(self, common_url, id_field=C.ID):
        resp = self.send_request('GET', common_url)
        obj_name = common_url.rpartition('/')[-1]
        RAsserts.assert_equal(resp.status_code, 200,
                              'Failed to get list of "{}" objects, manual clean-up may be required!!'
                              .format(obj_name))
        for obj in resp.json():
            obj_id = obj[id_field]
            resp = self.send_request('DELETE', '{}/{}'.format(common_url, obj_id))
            RAsserts.assert_equal(resp.status_code, 200,
                                  'Failed to delete {} {} object, manual clean-up may be required!!'
                                  .format(obj_id, obj_name))
